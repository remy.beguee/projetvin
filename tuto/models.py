# -*- coding: utf-8 -*-
from .app import db

from flask.ext.login import UserMixin
from .app import login_manager

class User(db.Model, UserMixin):
	pseudo = db.Column(db.String(100),primary_key=True)
	usermail = db.Column(db.String(100))
	username = db.Column(db.String(50))
	usersurname = db.Column(db.String(50))
	password = db.Column(db.String(64))
	
	def get_id(self):
		return self.pseudo
	
	def __repr__(self):
		return "%s - %s - %s" % (self.pseudo, self.username, self.usermail)

from .app import login_manager

@login_manager.user_loader
def load_user(usermail):
	return User.query.get(usermail)

	
class Vin(db.Model):
	categorie=db.Column(db.String(100))
	pays=db.Column(db.String(100))
	image=db.Column(db.String(150))
	nom=db.Column(db.String(100),primary_key=True)
	quantite=db.Column(db.String(5))
	region=db.Column(db.String(100))
	variete=db.Column(db.String(100))
	vintage=db.Column(db.String(10))
	
	def __repr__(self):
		return "%s" % (self.nom)
	
class Commande(db.Model):
	__tablename__='Commande'
	reference=db.Column(db.Integer,primary_key=True)
	client_reference=db.Column(db.String(100),db.ForeignKey("user.pseudo"))
	client=db.relationship("User",backref=db.backref("User",lazy="dynamic"))
	vin_reference=db.Column(db.String(100),db.ForeignKey("vin.nom"))
	client=db.relationship("User",backref=db.backref("User",lazy="dynamic"))
	vin=db.relationship("Vin",backref=db.backref("Vin",lazy="dynamic"))		
	quantite=db.Column(db.Integer)
	
	def __repr__(self):
		return "<Commande (%d)>" % (self.reference)


def get_sample():
	return Vin.query.all()
	
from flask.ext.admin import Admin, BaseView, expose

class MyView(BaseView):
    @expose('/')
    def index(self):
        return self.render('home.html')
